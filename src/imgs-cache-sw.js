// importScripts('ngsw-worker.js'); // automatically generated ngsw

// self.addEventListener('fetch', function(event) {
//   console.log('fetch event detected');
//   event.respondWith(
//     console.log('opening cache data');
//     caches.open('mysite-dynamic').then(function(cache) {
//       console.log('cache data opened');
//       return fetch(event.request).then(function(response) {
//         console.log('response received');
//         cache.put(event.request, response.clone());
//         return response;
//       });
//     })
//   );
// });

self.addEventListener('fetch', event => {
  console.log('fetch event detected');
  event.respondWith(caches.open('dynamic-imgs').then(cache => {
    console.log('cache data opened');
    return fetch(event.request).then(resp => {
      console.log('response received');
      cache.put(event.request, resp.clone());
      return resp;
    })
  }))
})

self.addEventListener('notificationclick', (event) => {
  console.log('notification clicked!')
});