importScripts('three.js');

onmessage = function (evt) {
    const type = evt.data.type;
    if (type === 'init') {
        const canvas = evt.data.canvas;
        const width = 200;
        const height = 200;
        console.log('width: ' + width);
        console.log('height: ' + height);

        var scene = new THREE.Scene();
        var camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

        var renderer = new THREE.WebGLRenderer({
            canvas
        });
        // canvas = renderer.domElement;
        // renderer.domElement.setSize( 200, 200);
        // canvas.appendChild(renderer.domElement);
        // document.body.appendChild( renderer.domElement );

        var geometry = new THREE.BoxGeometry(0.4, 0.4, 0.4);
        var material = new THREE.MeshPhongMaterial({
            color: new THREE.Color('rgb(255,96,70)')
        });
        var cube = new THREE.Mesh(geometry, material);
        scene.add(cube);
        const color = 0xFFFFFF;
        const intensity = 1;
        const light = new THREE.DirectionalLight(color, intensity);
        light.position.set(-1, 2, 4);
        scene.add(light);

        camera.position.z = 1;

        var animate = function () {
            requestAnimationFrame(animate);

            // cube.rotation.x += 0.01;
            cube.rotation.y += 0.03;

            renderer.render(scene, camera);
        };

        animate();

    } else {
        var _fibonacci = function(num) {
            if (num <= 1) return 1
            return _fibonacci(num - 1) + _fibonacci(num - 2)
        }

        _fibonacci(40);
        this.postMessage({res: 'Done!'})
    }
};