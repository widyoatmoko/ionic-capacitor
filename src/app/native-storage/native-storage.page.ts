import { Component, OnInit } from '@angular/core';
import { Plugins } from "@capacitor/core";

const { Storage } = Plugins;

export interface User {
  id: number;
  username: string;
}

@Component({
  selector: 'app-native-storage',
  templateUrl: './native-storage.page.html',
  styleUrls: ['./native-storage.page.scss'],
})

export class NativeStoragePage implements OnInit {
  user: User;
  username:string;

  constructor() { }

  ngOnInit() {
    this.getObject();
  }

  async setObject() {
    this.user = {
      id: 1,
      username: this.username
    }

    await Storage.set({
      key: 'user',
      value: JSON.stringify(this.user)
    });
  }

  async getObject() {
    const ret = await Storage.get({ key: 'user' });
    if(ret) {
      this.user = JSON.parse(ret.value);
    }
  }

  async removeItem() {
    delete this.user;
    await Storage.remove({ key: 'user' });
  }

}
