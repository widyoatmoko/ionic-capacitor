import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebPushNotificationPage } from './web-push-notification.page';

describe('WebPushNotificationPage', () => {
  let component: WebPushNotificationPage;
  let fixture: ComponentFixture<WebPushNotificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebPushNotificationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebPushNotificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
