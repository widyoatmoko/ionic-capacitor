import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WebPushNotificationPage } from './web-push-notification.page';

const routes: Routes = [
  {
    path: '',
    component: WebPushNotificationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WebPushNotificationPage]
})
export class WebPushNotificationPageModule {}
