import { FcmService } from './../fcm.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-web-push-notification',
  templateUrl: './web-push-notification.page.html',
  styleUrls: ['./web-push-notification.page.scss'],
})
export class WebPushNotificationPage implements OnInit {
  message: any;

  constructor(private fcmSvc: FcmService) { }

  ngOnInit() {
    const userId = 'user001';
    this.fcmSvc.requestPermission(userId)
    this.fcmSvc.receiveMessage()
    this.message = this.fcmSvc.currentMessage
  }

  sendNotification() {
    this.fcmSvc.sendFcm()
    .subscribe(res => {
      console.log('Result = ', JSON.stringify(res));
    }, err => {
      console.log('Error = ', JSON.stringify(err));
    })
  }
}
