import { CamvasService } from './../camvas.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { Camvas } from "./camvas";

@Component({
  selector: 'app-camera-canvas',
  templateUrl: './camera-canvas.page.html',
  styleUrls: ['./camera-canvas.page.scss'],
})
export class CameraCanvasPage implements OnInit {
  @ViewChild('canvas') canvasEl: ElementRef;


  constructor(private camvasSvc: CamvasService) { }

  ngOnInit() {
    // const ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
    const ctx = this.canvasEl.nativeElement.getContext('2d');
    // const ctx = cvs.getContext['2d'];
    let draw = (video, dt) => {
      ctx.drawImage(video, 0, 0)
    }
    let myCamvas = new this.camvasSvc.camvas(ctx, draw);
  }

  ngAfterViewInit() {
    
  }
}
