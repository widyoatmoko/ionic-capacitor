import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraCanvasPage } from './camera-canvas.page';

describe('CameraCanvasPage', () => {
  let component: CameraCanvasPage;
  let fixture: ComponentFixture<CameraCanvasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CameraCanvasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraCanvasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
