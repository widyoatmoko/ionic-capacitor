import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import * as faceapi from 'face-api.js';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-face-recognition',
  templateUrl: './face-recognition.page.html',
  styleUrls: ['./face-recognition.page.scss'],
})
export class FaceRecognitionPage implements OnInit {
  @ViewChild('face') faceVid;
  @ViewChild('overlay') overlayCanvas;
  @ViewChild('bbtImage') bbtImage;

  MODEL_URL = '/assets/weights'
  input: HTMLVideoElement;
  canvas: HTMLCanvasElement;
  loading: HTMLIonLoadingElement;
  isLoading = false;
  isDetecting = false;

  constructor(public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.input = this.faceVid.nativeElement;
    this.canvas = this.overlayCanvas.nativeElement;
    this.start();
  }

  async start() {
    this.loading = await this.loadingCtrl.create({
      message: 'Loading Model...'
    });
    await this.loading.present();
    Promise.all([
      console.log('load model'),
      faceapi.nets.tinyFaceDetector.loadFromUri(this.MODEL_URL),
      faceapi.nets.faceLandmark68Net.loadFromUri(this.MODEL_URL),
      faceapi.nets.faceRecognitionNet.loadFromUri(this.MODEL_URL),
      faceapi.nets.faceExpressionNet.loadFromUri(this.MODEL_URL),
    ]).then(async () => {
      await this.loading.dismiss();
      console.log('starting camera');
      this.startCam();
    }, async err => await console.error(err));
  }

  async doDetection() {
    if (this.isDetecting) {
      this.loading = await this.loadingCtrl.create({
        message: 'Detecting face...'
      });
      await this.loading.present();
      this.isLoading = true;
      console.log('do detection...');
      requestAnimationFrame(this.detectFace.bind(this));
    }
    // this.detectFace();
  }

  async detectFace() {
    if (this.isDetecting) {
      let fullFaceDescriptions = await faceapi.detectSingleFace(this.input, new faceapi.TinyFaceDetectorOptions()).withFaceExpressions();
      if (fullFaceDescriptions) {
        const dims = faceapi.matchDimensions(this.canvas, this.input, true);
        const resizedResults = faceapi.resizeResults(fullFaceDescriptions, dims);
        faceapi.draw.drawDetections(this.canvas, resizedResults);
        faceapi.draw.drawFaceExpressions(this.canvas, resizedResults);
        if (this.isLoading) {
          this.loading.dismiss();
          this.isLoading = false;
        }
      } else {
        console.log('no face detected');
        if (this.isLoading) {
          this.loading.dismiss();
          this.isLoading = false;
        }
      }
      requestAnimationFrame(this.detectFace.bind(this));
    }
    // requestAnimationFrame(this.detectFace.bind(this));
  }


  startCam() {
    this.input.setAttribute('playsinline', '');
    // The callback happens when we are starting to stream the video.
    navigator.mediaDevices.getUserMedia(
      { video: {} }).then(async stream => {
        // Yay, now our webcam input is treated as a normal video and
        // we can start having fun
        try {
          console.log("Trying with srcObject");
          this.input.srcObject = stream;
        } catch (error) {
          console.error("Trying failed with error;");
          console.dir(error);
          this.input.src = URL.createObjectURL(stream);
        }
      }, err => {
        throw err;
      })
  }

  toggleDetection() {
    console.log("Detection: " + this.isDetecting);
    this.isDetecting = !this.isDetecting;
    console.log("Detection: " + this.isDetecting);
    this.doDetection();
  }

}
