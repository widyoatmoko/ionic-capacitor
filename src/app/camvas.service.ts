import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CamvasService {

  constructor() { }


  camvas(ctx, draw) {
    // We can't `new Video()` yet, so we'll resort to the vintage
    // "hidden div" hack for dynamic loading.
    const streamContainer = document.createElement('div');
    const video = document.createElement('video');

    // If we don't do this, the stream will not be played.
    // By the way, the play and pause controls work as usual 
    // for streamed videos.
    video.setAttribute('autoplay', '1');

    // The video should fill out all of the canvas
    video.setAttribute('width', ctx.canvas.width);
    video.setAttribute('height', ctx.canvas.height);

    video.setAttribute('style', 'display:none');

    // The video should not go full screen for ios
    video.setAttribute( 'playsinline', '');
    streamContainer.appendChild(video)
    document.body.appendChild(streamContainer)

    // The callback happens when we are starting to stream the video.
    navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
      // Yay, now our webcam input is treated as a normal video and
      // we can start having fun
      try {
        video.srcObject = stream;
      } catch (error) {
        video.src = URL.createObjectURL(stream);
      }
      // Let's start drawing the canvas!
      update();
    }, err => {
      throw err;
    })

    // As soon as we can draw a new frame on the canvas, we call the `draw` function 
    // we passed as a parameter.
    let update = () => {
      let last = Date.now();
      // For some effects, you might want to know how much time is passed
      // since the last frame; that's why we pass along a Delta time `dt`
      // variable (expressed in milliseconds)
      let loop = () => {
        const dt = Date.now() - last;
        // const dt = 1000/2;
        console.log('fps = ', 1000/dt);
        draw(video, dt);
        last = Date.now();
        requestAnimationFrame(loop);
      };
      requestAnimationFrame(loop);
    };
  }
}
