import { Component, OnInit } from '@angular/core';
import { Plugins } from "@capacitor/core";

const { Network } = Plugins

@Component({
  selector: 'app-offline-detection',
  templateUrl: './offline-detection.page.html',
  styleUrls: ['./offline-detection.page.scss'],
})
export class OfflineDetectionPage implements OnInit {
  status: any;
  networkListener: any;
  isListening = false;

  constructor() { }

  ngOnInit() {
    Network.getStatus().then(status => {
      this.status = JSON.stringify(status);
    });
  }

  startListening() {
    this.isListening = true;
    this.networkListener = Network.addListener('networkStatusChange', (status) => {
      console.log("Network status changed", status);
      this.status = JSON.stringify(status); 
      alert("Offline Detection\n" + JSON.stringify(status));
    });

  }

  stopListening() {
    this.isListening = false;
    this.networkListener.remove();
  }
}
