import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OfflineDetectionPage } from './offline-detection.page';

const routes: Routes = [
  {
    path: '',
    component: OfflineDetectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OfflineDetectionPage]
})
export class OfflineDetectionPageModule {}
