import { Component, OnInit } from '@angular/core';
// const data = require('./data.json');
import { Data } from "./data";


@Component({
  selector: 'app-image-cache',
  templateUrl: './image-cache.page.html',
  styleUrls: ['./image-cache.page.scss'],
})
export class ImageCachePage implements OnInit {
  data = Data;
  items = this.data['items'];

  constructor() { }

  ngOnInit() {
    console.dir(this.items);
    
  }

  // get items():Array<string>{
  //   return Data['items'];
  // }

}
