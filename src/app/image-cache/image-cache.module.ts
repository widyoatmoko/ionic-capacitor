import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ImageCachePage } from './image-cache.page';
import { ImageCacheDirective } from './image-cache.directive';

const routes: Routes = [
  {
    path: '',
    component: ImageCachePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ImageCachePage, ImageCacheDirective]
})
export class ImageCachePageModule {}
