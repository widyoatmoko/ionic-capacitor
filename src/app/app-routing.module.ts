import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'camera',
    loadChildren: './camera/camera.module#CameraPageModule'
  },
  { path: 'push-notification', loadChildren: './push-notification/push-notification.module#PushNotificationPageModule' },
  { path: 'local-notification', loadChildren: './local-notification/local-notification.module#LocalNotificationPageModule' },
  { path: 'offline-detection', loadChildren: './offline-detection/offline-detection.module#OfflineDetectionPageModule' },
  { path: 'native-storage', loadChildren: './native-storage/native-storage.module#NativeStoragePageModule' },
  { path: 'toast', loadChildren: './toast/toast.module#ToastPageModule' },
  { path: 'camera-canvas', loadChildren: './camera-canvas/camera-canvas.module#CameraCanvasPageModule' },
  { path: 'native-http', loadChildren: './native-http/native-http.module#NativeHttpPageModule' },
  { path: 'web-push-notification', loadChildren: './web-push-notification/web-push-notification.module#WebPushNotificationPageModule' },
  { path: 'status-bar', loadChildren: './status-bar/status-bar.module#StatusBarPageModule' },
  { path: 'app-plugin', loadChildren: './app-plugin/app-plugin.module#AppPluginPageModule' },
  { path: 'image-cache', loadChildren: './image-cache/image-cache.module#ImageCachePageModule' },
  { path: 'filesystem', loadChildren: './filesystem/filesystem.module#FilesystemPageModule' },
  { path: 'google-sign-in', loadChildren: './google-sign-in/google-sign-in.module#GoogleSignInPageModule' },
  { path: 'web-gl', loadChildren: './web-gl/web-gl.module#WebGLPageModule' },
  { path: 'serviceworker-cache', loadChildren: './serviceworker-cache/serviceworker-cache.module#ServiceworkerCachePageModule' },
  { path: 'offscreen-canvas', loadChildren: './offscreen-canvas/offscreen-canvas.module#OffscreenCanvasPageModule' },
  { path: 'sygic-clone', loadChildren: './sygic-clone/sygic-clone.module#SygicClonePageModule' },
  { path: 'face-recognition', loadChildren: './face-recognition/face-recognition.module#FaceRecognitionPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
