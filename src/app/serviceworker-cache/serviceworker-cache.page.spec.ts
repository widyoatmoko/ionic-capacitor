import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceworkerCachePage } from './serviceworker-cache.page';

describe('ServiceworkerCachePage', () => {
  let component: ServiceworkerCachePage;
  let fixture: ComponentFixture<ServiceworkerCachePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceworkerCachePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceworkerCachePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
