import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiceworkerCachePage } from './serviceworker-cache.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceworkerCachePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiceworkerCachePage]
})
export class ServiceworkerCachePageModule {}
