import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebGLPage } from './web-gl.page';

describe('WebGLPage', () => {
  let component: WebGLPage;
  let fixture: ComponentFixture<WebGLPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebGLPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebGLPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
