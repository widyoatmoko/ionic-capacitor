import { Component, OnInit, ViewChild } from '@angular/core';
import { Plugins } from "@capacitor/core";

const { Toast } = Plugins


@Component({
  selector: 'app-web-gl',
  templateUrl: './web-gl.page.html',
  styleUrls: ['./web-gl.page.scss'],
})
export class WebGLPage implements OnInit {
  @ViewChild ('threadCanvas') threadCanvas;
  offscreenAvailable = false;
  worker: Worker;
  buttonMainText = "Make main thread busy!"
  buttonWorkerText = "Make worker thread busy!"

  constructor() { }

  ngOnInit() {
    this.runOffscreenCanvas();
  }

  runOffscreenCanvas() {
    // const mainCtx = this.mainCanvas.nativeElement.getContext('2d');
    const threadEl = this.threadCanvas.nativeElement
    if('transferControlToOffscreen' in threadEl) {
      this.offscreenAvailable = true;
      const offscreen = threadEl.transferControlToOffscreen();
      this.worker = new Worker("webgl-worker.js"); 
      onload => {
        const myCanvas = this.threadCanvas.nativeElement
        console.log('width: ' + myCanvas.clientWidth);
        console.log('height: ' + myCanvas.clientHeight);
      }
      this.worker.postMessage({type: 'init', canvas: offscreen,
        width: threadEl.clientWidth,
        height: threadEl.clientHeight
      }, [offscreen]);
    } else {
      this.offscreenAvailable = false;
    }

  }

  async busyMe() {
    this.buttonMainText = "I'm busy!"
    console.log("let's get busy");
    setTimeout(async () => {
      this._fibonacci(40);
      console.log("done");
      this.buttonMainText = "Make main thread busy!"
      await Toast.show({
        text: 'Done!'
      })
    }, 200);
  }

  async busyThread() {
    if (this.offscreenAvailable) {
      this.buttonWorkerText = "I'm busy!"
      console.log("let's get busy");
      this.worker.postMessage({type: 'getBusy'});
      this.worker.onmessage = async ({data}) => {
        this.buttonWorkerText = "Make worker thread busy!"
        await Toast.show({
          text: data.res
        })
      }
    }
  }

  _fibonacci(num) {
    if (num <= 1) return 1
  
    return this._fibonacci(num - 1) + this._fibonacci(num - 2)
  }
}
