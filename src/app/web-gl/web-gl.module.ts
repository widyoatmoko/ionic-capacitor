import { SpinningCubeComponent } from './spinning-cube/spinning-cube.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WebGLPage } from './web-gl.page';

const routes: Routes = [
  {
    path: '',
    component: WebGLPage,
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WebGLPage, SpinningCubeComponent]
})
export class WebGLPageModule {}
