import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Plugins } from "@capacitor/core";

const { Toast } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class FcmService {
  token: any;

  // constructor(private http: HTTP) { }
  constructor(
    // private angularFireDB: AngularFireDatabase,
    // private angularFireAuth: AngularFireAuth,
    private http: HttpClient,
    private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    )
  }

  sendFcm() {
    const fcmUrl = "https://fcm.googleapis.com/fcm/send";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'key=AAAAB1yQaTY:APA91bGvTCNyf9sB2FOrk7yrWENFOnbsA9DwqgNmk5pRVgedeT3B4BC2BqwH-o2gSJAczjuNkTjM7SxygwtmV39AHpOcd3zfFqRPE9gtkzlSQYUnQAJrlB3Yb7Bl2kmJz732dK3IxC2H'
      })
    };

    return this.http.post(fcmUrl, {
      "notification": {
        "title": "Web Push Notification",
        "body": "Firebase is awesome",
        "click_action": "http://pwa.waikiindonesia.com/",
        "icon": "https://pwa.waikiindonesia.com/assets/icon/favicon.png"
      },
      "to": this.token
    }, httpOptions);
  }


  currentMessage = new BehaviorSubject(null);



  // /**
  //  * update token in firebase database
  //  * 
  //  * @param userId userId as a key 
  //  * @param token token as a value
  //  */
  // updateToken(userId, token) {
  //   // we can change this function to request our backend service
  //   this.angularFireAuth.authState.pipe(take(1)).subscribe(
  //     () => {
  //       const data = {};
  //       data[userId] = token
  //       this.angularFireDB.object('fcmTokens/').update(data)
  //     })
  // }

  /**
   * request permission for notification from firebase cloud messaging
   * 
   * @param userId userId
   */
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.token = token;
        // this.updateToken(userId, token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
        const fcmPayload:any = payload;
        this.showToast(fcmPayload.notification.body);
        this.currentMessage.next(payload);
      })
  }

  async showToast(msg) {
    await Toast.show({
      text: msg
    });
  }
}
