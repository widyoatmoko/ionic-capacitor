import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Camera',
      url: '/camera',
      icon: 'camera'
    },
    {
      title: 'Push Notification',
      url: '/push-notification',
      icon: 'notifications'
    },
    {
      title: 'Local Notification',
      url: '/local-notification',
      icon: 'notifications'
    },
    {
      title: 'Offline Detection',
      url: '/offline-detection',
      icon: 'wifi'
    },
    {
      title: 'Native Storage',
      url: '/native-storage',
      icon: 'save'
    },
    {
      title: 'Toast',
      url: '/toast',
      icon: 'information-circle'
    },
    {
      title: 'Camera Canvas',
      url: '/camera-canvas',
      icon: 'camera'
    },
    {
      title: 'Native Http',
      url: '/native-http',
      icon: 'globe'
    },
    {
      title: 'Web Push Notification',
      url: '/web-push-notification',
      icon: 'notifications'
    },
    {
      title: 'Status Bar',
      url: '/status-bar',
      icon: 'apps'
    },
    {
      title: 'App Plugin',
      url: '/app-plugin',
      icon: 'apps'
    },
    {
      title: 'Image Cache',
      url: '/image-cache',
      icon: 'image'
    },
    {
      title: 'Service Worker Cache',
      url: '/serviceworker-cache',
      icon: 'image'
    },
    {
      title: 'Filesystem',
      url: '/filesystem',
      icon: 'apps'
    },
    {
      title: 'Google Sign-In',
      url: '/google-sign-in',
      icon: 'apps'
    },
    {
      title: 'WebGL',
      url: '/web-gl',
      icon: 'apps'
    },
    {
      title: 'OffscreenCanvas',
      url: '/offscreen-canvas',
      icon: 'image'
    },
    {
      title: 'Sygic Clone',
      url: '/sygic-clone',
      icon: 'map'
    },
    {
      title: 'Face Recognition',
      url: '/face-recognition',
      icon: 'map'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private updates: SwUpdate,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.updates.available.subscribe(event => {
      console.log('current version is', event.current);
      console.log('available version is', event.available);
      if(confirm("New version available. Load New Version?")) {
        window.location.reload();
    }
      // alert('Update is available.\nReload to update!')
    });
  }
}
