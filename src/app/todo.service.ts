import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  BASE_URL = "https://jsonplaceholder.typicode.com";
  // BASE_URL = 'http://ulp.patikab.go.id/api_devel/api/evaluasi/list_paket_penawaran/'

  constructor(
    private nativeHttp: HTTP,
    // private http: HttpClient
  ) { }

  getTodoList() {
    let httpUrl = this.BASE_URL + "/todos";
    // let httpUrl = this.BASE_URL;
    return this.nativeHttp.get(httpUrl, {}, {});
  }

  getTodoDetail(id) {
    let httpUrl = this.BASE_URL + "/todos/" + id;
    return this.nativeHttp.get(httpUrl, {}, {});
  }

}
