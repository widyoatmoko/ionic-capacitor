import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';


@Component({
  selector: 'app-sygic-clone',
  templateUrl: './sygic-clone.page.html',
  styleUrls: ['./sygic-clone.page.scss'],
})
export class SygicClonePage implements OnInit {
  @ViewChild('globeCanvas') globeCanvas;
  windowWidth  = window.innerWidth;
  windowHeight = window.innerHeight;
  width  = window.innerWidth > window.innerHeight? window.innerHeight: window.innerWidth ;
  height = window.innerWidth > window.innerHeight? window.innerHeight: window.innerWidth ;

  scene = new THREE.Scene();

  geometry  : THREE.SphereGeometry;
  material  : THREE.MeshPhongMaterial;
  earthMesh : THREE.Mesh;
  camera    : THREE.PerspectiveCamera;
  light     : THREE.DirectionalLight;

  renderer: THREE.WebGLRenderer;
  controls: OrbitControls;

  fps: string;
  totalFps = 0;
  frameCount = 0;
  then = 0;


  constructor() { }

  ngOnInit() {

    console.log('width: ' + this.width + " + " + this.windowWidth);
    console.log('height: ' + this.height + " + " + this.windowHeight);
    this.initGlobe();
  }


  initGlobe() {
    const radius = window.innerWidth > window.innerHeight? window.innerHeight/window.innerWidth: window.innerWidth/window.innerHeight ;
    this.geometry  = new THREE.SphereGeometry(radius/2, 32, 32)
    this.material  = new THREE.MeshPhongMaterial({
      map : THREE.ImageUtils.loadTexture('assets/sygic/earthmap1k.jpg'),
      bumpMap: THREE.ImageUtils.loadTexture('assets/sygic/earthbump1k.jpg'),
      bumpScale: 0.005,
      specularMap: THREE.ImageUtils.loadTexture('assets/sygic/earthspec1k.png'),
      specular: new THREE.Color(0x050501)
    });
    this.earthMesh = new THREE.Mesh(this.geometry, this.material)

    this.renderer = new THREE.WebGLRenderer({antialias: false, canvas: this.globeCanvas.nativeElement});
    this.renderer.setSize(this.windowWidth, this.windowHeight);

    this.scene.add(this.earthMesh)
    this.scene.add(new THREE.AmbientLight(0x555555));

    this.camera = new THREE.PerspectiveCamera(45, this.windowWidth / this.windowHeight, 0.01, 1000);
    this.camera.position.z = 1.5;

    this.light = new THREE.DirectionalLight(0xffffff, 1.5);
    this.light.position.set(5,1,4);
    this.scene.add(this.light);

    this.controls = new OrbitControls(this.camera);
    this.render();
    requestAnimationFrame(this.fpsRender.bind(this));
  }

  render() {
    this.controls.update();
    this.earthMesh.rotation.y += 0.003;
    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(this.render.bind(this));
  }


  @HostListener ('window:resize', ['$event'])
    onResize(event) {
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;

    this.camera.aspect = this.windowWidth/this.windowHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.windowWidth, this.windowHeight);
    this.renderer.render(this.scene, this.camera);
  }


  fpsRender(now) {
    this.frameCount++;
    now *= 0.001;                          // convert to seconds
    const deltaTime = now - this.then;     // compute time since last frame
    this.then = now;                       // remember time for next frame
    const fps = 1 / deltaTime;             // compute frames per second

    this.totalFps += fps;                  // count total fps
    const avgFps = this.totalFps / this.frameCount;
    this.fps = avgFps.toFixed(0);             // update fps display
    requestAnimationFrame(this.fpsRender.bind(this));
  }

}
