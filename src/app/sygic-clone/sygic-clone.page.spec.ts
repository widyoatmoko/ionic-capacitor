import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SygicClonePage } from './sygic-clone.page';

describe('SygicClonePage', () => {
  let component: SygicClonePage;
  let fixture: ComponentFixture<SygicClonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SygicClonePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SygicClonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
