import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleSignInPage } from './google-sign-in.page';

describe('GoogleSignInPage', () => {
  let component: GoogleSignInPage;
  let fixture: ComponentFixture<GoogleSignInPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleSignInPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleSignInPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
