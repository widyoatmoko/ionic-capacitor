import { Component, OnInit } from '@angular/core';
import { Plugins } from "@capacitor/core";
import "@codetrix-studio/capacitor-google-auth";

const { GoogleAuth } = Plugins;

@Component({
  selector: 'app-google-sign-in',
  templateUrl: './google-sign-in.page.html',
  styleUrls: ['./google-sign-in.page.scss'],
})
export class GoogleSignInPage implements OnInit {
  // googleUser: any;
  name: string;
  email: string;
  imageUrl: string;

  constructor() { }

  ngOnInit() {
  }

  login() {
    this._googleSignIn();
  }

  logout() {
    this._googleSignOut();
  }

  async _googleSignIn() {
    let googleUser = await GoogleAuth.signIn();
    console.log('google user = ' + JSON.stringify(googleUser));
    this.email = googleUser.email;
    this.name = googleUser.name;
    this.imageUrl = googleUser.imageUrl;
    // try {
    //   console.log('google user = ' + JSON.stringify(googleUser));
    // } catch (error) {
    //   console.log('Error = ' + error);
    //   console.dir(googleUser);
    // }
    // let profile = await googleUser.getBasicProfile();
    // console.log('google user = ' + JSON.stringify(profile));
  }

  async _googleSignOut() {
    let googleUser = await GoogleAuth.signOut();
    console.log('google user = ' + JSON.stringify(googleUser));
    delete this.email;
    delete this.name;
    // this.googleUser = await GoogleAuth.signIn();
    // let status = await this.googleUser.signOut();
    // console.log('logout status = ' + JSON.stringify(status));
  }

}
