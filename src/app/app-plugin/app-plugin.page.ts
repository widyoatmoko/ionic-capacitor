import { Component, OnInit } from '@angular/core';
import { Plugins } from "@capacitor/core";

const { App } = Plugins;

@Component({
  selector: 'app-app-plugin',
  templateUrl: './app-plugin.page.html',
  styleUrls: ['./app-plugin.page.scss'],
})
export class AppPluginPage implements OnInit {

  constructor() { }

  ngOnInit() {
    App.addListener('backButton', data => {
      console.log('App opened with URL: ' +  data.url);
      alert('Back button pressed!')
    })
  }

  exitApp() {
    alert('Exit button pressed!')
    App.exitApp();
  }

}
