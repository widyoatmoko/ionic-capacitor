import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPluginPage } from './app-plugin.page';

describe('AppPluginPage', () => {
  let component: AppPluginPage;
  let fixture: ComponentFixture<AppPluginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppPluginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPluginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
