import { TodoService } from './../todo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-native-http',
  templateUrl: './native-http.page.html',
  styleUrls: ['./native-http.page.scss'],
})
export class NativeHttpPage implements OnInit {
  todos: any;

  constructor(private todoSvc: TodoService) { }

  ngOnInit() {
    this.todoSvc.getTodoList().then(data => {
      this.todos = JSON.parse(data.data);
    })
  }

}
