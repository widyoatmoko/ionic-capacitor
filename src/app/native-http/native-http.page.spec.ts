import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NativeHttpPage } from './native-http.page';

describe('NativeHttpPage', () => {
  let component: NativeHttpPage;
  let fixture: ComponentFixture<NativeHttpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NativeHttpPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NativeHttpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
