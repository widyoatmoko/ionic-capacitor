import { TestBed } from '@angular/core/testing';

import { CamvasService } from './camvas.service';

describe('CamvasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CamvasService = TestBed.get(CamvasService);
    expect(service).toBeTruthy();
  });
});
