import { Component, OnInit } from '@angular/core';
import { Plugins, StatusBarStyle } from "@capacitor/core";

const { StatusBar } = Plugins;

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.page.html',
  styleUrls: ['./status-bar.page.scss'],
})
export class StatusBarPage implements OnInit {
  isStatusBarLight = true

  constructor() { }

  ngOnInit() {
  }

  changeStatusBar() {
    StatusBar.setStyle({
      style: this.isStatusBarLight ? StatusBarStyle.Dark : StatusBarStyle.Light
    });
    this.isStatusBarLight = !this.isStatusBarLight;
  }

  hideStatusBar() {
    StatusBar.hide();
  }

  showStatusBar() {
    StatusBar.show();
  }

  setBackgroundColor() {
    const bits = [0, 0, 0];
    const randomColor = bits.map(b => {
      const v = Math.floor(Math.random() * 0xff).toString(16);
      if (v.length < 2) {
        return '0' + v;
      }
      return v;
    }).join('');
    console.log('Random color: ', randomColor);
    StatusBar.setBackgroundColor({ color: '#000000' })
  }
}
