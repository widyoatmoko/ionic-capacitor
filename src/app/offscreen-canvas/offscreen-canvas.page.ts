import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Plugins } from "@capacitor/core";

const { Toast } = Plugins

@Component({
  selector: 'app-offscreen-canvas',
  templateUrl: './offscreen-canvas.page.html',
  styleUrls: ['./offscreen-canvas.page.scss'],
})
export class OffscreenCanvasPage implements OnInit {
  @ViewChild('mainCanvas') mainCanvas: ElementRef;
  @ViewChild('threadCanvas') threadCanvas: ElementRef;

  offscreenAvailable = true;

  sun = new Image();
  moon = new Image();
  earth = new Image();

  constructor() { }

  ngOnInit() {
    this.sun.src = "/assets/offscreen-canvas/Canvas_sun.png";
    this.moon.src = "/assets/offscreen-canvas/Canvas_moon.png";
    this.earth.src = "/assets/offscreen-canvas/Canvas_earth.png";
    requestAnimationFrame(this.draw.bind(this));
    // requestAnimationFrame(this.drawThread.bind(this));
    this.runOffscreenCanvas();


  }

  draw() {
    const mainCtx = this.mainCanvas.nativeElement.getContext('2d');
    // const threadCtx = this.threadCanvas.nativeElement.getContext('2d');
    // const canvasEl: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('mainCanvas');
    // const mainCtx = canvasEl.getContext('2d');


    mainCtx.globalCompositeOperation = 'destination-over';
    mainCtx.clearRect(0, 0, 200, 200); // clear canvas
  
    mainCtx.fillStyle = 'rgba(0, 0, 0, 0.4)';
    mainCtx.strokeStyle = 'rgba(0, 153, 255, 0.4)';
    mainCtx.save();
    mainCtx.translate(100, 100);

    // Earth
    var time = new Date();
    mainCtx.rotate(((2 * Math.PI) / 60) * time.getSeconds() + ((2 * Math.PI) / 60000) * time.getMilliseconds());
    mainCtx.translate(70, 0);
    mainCtx.fillRect(0, -12, 40, 24); // Shadow
    mainCtx.drawImage(this.earth, -12, -12);

    // Moon
    mainCtx.save();
    mainCtx.rotate(((2 * Math.PI) / 6) * time.getSeconds() + ((2 * Math.PI) / 6000) * time.getMilliseconds());
    mainCtx.translate(0, 21.5);
    mainCtx.drawImage(this.moon, -3.5, -3.5);
    mainCtx.restore();

    mainCtx.restore();
    
    mainCtx.beginPath();
    mainCtx.arc(100, 100, 70, 0, Math.PI * 2, false); // Earth orbit
    mainCtx.stroke();
  
    mainCtx.drawImage(this.sun, 0, 0, 200, 200);

    requestAnimationFrame(this.draw.bind(this));
  }

  // drawThread() {
  //   // const mainCtx = this.mainCanvas.nativeElement.getContext('2d');
  //   const threadEl = this.threadCanvas.nativeElement
  //   const threadCtx = this.threadCanvas.nativeElement.getContext('2d');
  //   // const canvasEl: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('mainCanvas');
  //   // const mainCtx = canvasEl.getContext('2d');


  //   threadCtx.globalCompositeOperation = 'destination-over';
  //   threadCtx.clearRect(0, 0, 300, 300); // clear canvas
  
  //   threadCtx.fillStyle = 'rgba(0, 0, 0, 0.4)';
  //   threadCtx.strokeStyle = 'rgba(0, 153, 255, 0.4)';
  //   threadCtx.save();
  //   threadCtx.translate(150, 150);

  //   // Earth
  //   var time = new Date();
  //   threadCtx.rotate(((2 * Math.PI) / 60) * time.getSeconds() + ((2 * Math.PI) / 60000) * time.getMilliseconds());
  //   threadCtx.translate(105, 0);
  //   threadCtx.fillRect(0, -12, 40, 24); // Shadow
  //   threadCtx.drawImage(this.earth, -12, -12);

  //   // Moon
  //   threadCtx.save();
  //   threadCtx.rotate(((2 * Math.PI) / 6) * time.getSeconds() + ((2 * Math.PI) / 6000) * time.getMilliseconds());
  //   threadCtx.translate(0, 28.5);
  //   threadCtx.drawImage(this.moon, -3.5, -3.5);
  //   threadCtx.restore();

  //   threadCtx.restore();
    
  //   threadCtx.beginPath();
  //   threadCtx.arc(150, 150, 105, 0, Math.PI * 2, false); // Earth orbit
  //   threadCtx.stroke();
  
  //   threadCtx.drawImage(this.sun, 0, 0, 300, 300);

  //   const offscreen = threadEl.transferControlToOffscreen();
  //   const worker = new Worker("my-offscreencanvas.js"); 
  //   worker.postMessage({canvas: offscreen}, [offscreen]);
  //   // requestAnimationFrame(this.drawThread.bind(this));
  // }

  runOffscreenCanvas() {
    // const mainCtx = this.mainCanvas.nativeElement.getContext('2d');
    const threadEl = this.threadCanvas.nativeElement
    if('transferControlToOffscreen' in threadEl) {
      this.offscreenAvailable = true;
      const offscreen = threadEl.transferControlToOffscreen();
      const worker = new Worker("my-offscreencanvas.js"); 
      worker.postMessage({canvas: offscreen}, [offscreen]);
    } else {
      this.offscreenAvailable = false;
    }

  }

  async busyMe() {
    console.log("let's get busy");
    this._fibonacci(40);
    console.log("done");
    await Toast.show({
      text: 'Done!'
    })
  }

  _fibonacci(num) {
    if (num <= 1) return 1
  
    return this._fibonacci(num - 1) + this._fibonacci(num - 2)
  }
}
