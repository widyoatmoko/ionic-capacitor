import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffscreenCanvasPage } from './offscreen-canvas.page';

describe('OffscreenCanvasPage', () => {
  let component: OffscreenCanvasPage;
  let fixture: ComponentFixture<OffscreenCanvasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffscreenCanvasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffscreenCanvasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
