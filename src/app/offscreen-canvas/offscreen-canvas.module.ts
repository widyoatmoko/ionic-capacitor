import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OffscreenCanvasPage } from './offscreen-canvas.page';

const routes: Routes = [
  {
    path: '',
    component: OffscreenCanvasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OffscreenCanvasPage]
})
export class OffscreenCanvasPageModule {}
