onmessage = function(evt) {
    const canvas = evt.data.canvas;
    var angle = 0

    // const sun = evt.data.sun;
    // const moon = evt.data.moon;
    // const earth = evt.data.earth;
    // const gl = canvas.getContext("webgl");
    // const threadCtx = canvas.getContext('2d');
    // var sun = new Image(); // this would not work, with error 'Image is not defined'
    // const sunBlob = fetch("/assets/offscreen-canvas/Canvas_sun.png").then(r => r.blob());
    // const sun = await createImageBitmap(sunBlob);
    // const moonBlob = await fetch("/assets/offscreen-canvas/Canvas_moon.png").then(r => r.blob());
    // const moon = await createImageBitmap(moonBlob);
    // const earthBlob = await fetch("/assets/offscreen-canvas/Canvas_earth.png").then(r => r.blob());
    // const earth = await createImageBitmap(earthBlob);


    function init() {
        requestAnimationFrame(draw);
    }

    function draw() {
        var ctx = canvas.getContext('2d');
      
        ctx.globalCompositeOperation = 'destination-over';
        ctx.clearRect(0, 0, 200, 200); // clear canvas
      
        ctx.fillStyle = '#EEEEEE';
        
        //draw the circle
        ctx.beginPath();

        var radius = 20 + 70 * Math.abs(Math.cos(angle));
        ctx.arc(
            100,
            100,
            radius,
            0,
            Math.PI * 2,
            false
        );
        ctx.closePath();

          // color in the circle
        ctx.fillStyle = '#006699'
        ctx.fill()

        angle += Math.PI / 64

        requestAnimationFrame(draw);
    }
      
    // function draw() {
    //     var ctx = canvas.getContext('webgl');
      
    //     ctx.globalCompositeOperation = 'destination-over';
    //     ctx.clearRect(0, 0, 300, 300); // clear canvas
      
    //     ctx.fillStyle = 'rgba(0, 0, 0, 0.4)';
    //     ctx.strokeStyle = 'rgba(0, 153, 255, 0.4)';
    //     ctx.save();
    //     ctx.translate(150, 150);
      
    //     // Earth
    //     var time = new Date();
    //     ctx.rotate(((2 * Math.PI) / 60) * time.getSeconds() + ((2 * Math.PI) / 60000) * time.getMilliseconds());
    //     ctx.translate(105, 0);
    //     ctx.fillRect(0, -12, 40, 24); // Shadow
    //     ctx.drawImage(earth, -12, -12);
      
    //     // Moon
    //     ctx.save();
    //     ctx.rotate(((2 * Math.PI) / 6) * time.getSeconds() + ((2 * Math.PI) / 6000) * time.getMilliseconds());
    //     ctx.translate(0, 28.5);
    //     ctx.drawImage(moon, -3.5, -3.5);
    //     ctx.restore();
      
    //     ctx.restore();
        
    //     ctx.beginPath();
    //     ctx.arc(150, 150, 105, 0, Math.PI * 2, false); // Earth orbit
    //     ctx.stroke();
       
    //     ctx.drawImage(sun, 0, 0, 300, 300);
      
    //     window.requestAnimationFrame(draw);
    //   }
      
      init()
  };
