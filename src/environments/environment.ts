// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB7Ng1wtCpPgneqcASofMh_1mehI7Wc1cE",
    authDomain: "ionic-capacitor-43141.firebaseapp.com",
    databaseURL: "https://ionic-capacitor-43141.firebaseio.com",
    projectId: "ionic-capacitor-43141",
    storageBucket: "ionic-capacitor-43141.appspot.com",
    messagingSenderId: "31617739062",
    appId: "1:31617739062:web:9e6fa31bb1b3a5ba"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
